﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    void OnDestroy()
    {
        foreach(Transform child in transform)
        {
            child.SetParent(null);
        }
    }
}
