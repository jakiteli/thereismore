﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public bool controlled = false;
    public GameObject projectilePrefab;
    public Transform projectileSpawn;
    public GameObject tilePrefab;
    public Transform terrain;

    public Sprite normalSprite;
    public Sprite crouchSprite;

    public BoxCollider2D boxCollider;

    public AudioClip jumpSound;
    public AudioClip crouchSound;
    public AudioClip shootingSound;
    public AudioClip grabSound;
    public AudioClip createTileSound;

    GameObject tileObj;
    float tileLifeTime = 2f;

    float colHeightNormal = 0.9f;
    float colHeightCrouch = 0.4f;
    float colOffsetCrouch = -0.25f;

    float projectileSpeed = 4f;
    bool shootAble = true;
    float shootingCd = 0.3f;

    AudioSource audioSource;
    DirectionController dir;
    SpriteRenderer sprite;
    Rigidbody2D rb;
    bool onGround;
    bool crouched;
    float maxSpeed = 3f;
    float jumpSpeed = 5f;
    Vector3 startPos;



    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        dir = GetComponent<DirectionController>();
        sprite = GetComponent<SpriteRenderer>();
        audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        startPos = transform.position;
    }

    void Update()
    {
        if(!controlled)
            return;

        MovePlayer();
    }

    void MovePlayer()
    {
        if(Input.GetKey(KeyCode.A) && PlayerInputs.instance.enableA)
        {
            if(rb.velocity.x > -maxSpeed)
                rb.velocity -= new Vector2(1f, 0f);
        }
        else if(Input.GetKey(KeyCode.D) && PlayerInputs.instance.enableD)
        {
            if(rb.velocity.x < maxSpeed)
                rb.velocity += new Vector2(1f, 0f);
        }
        else
        {
            if(rb.velocity.x < 0.1f)
                rb.velocity = new Vector2(0f, rb.velocity.y);

            if(rb.velocity.x > 0f)
                rb.velocity -= new Vector2(1f, 0f);
            else if(rb.velocity.x < 0f)
                rb.velocity += new Vector2(1f, 0f);
        }

        if(Input.GetKeyDown(KeyCode.W) && PlayerInputs.instance.enableW)
        {
            if(onGround)
            {
                PlaySound(jumpSound);
                rb.velocity = new Vector2(rb.velocity.x, jumpSpeed);
                onGround = false;
            }
        }

        if(Input.GetKeyDown(KeyCode.S) && PlayerInputs.instance.enableS)
            ChangeSize(true);
        if(Input.GetKeyUp(KeyCode.S) && PlayerInputs.instance.enableS)
            ChangeSize(false);

        if(Input.GetKeyDown(KeyCode.Space) && PlayerInputs.instance.enableSpace)
            Shoot();

        if(Input.GetKeyDown(KeyCode.Mouse0) && PlayerInputs.instance.enableM1)
            Mouse1();
        if(Input.GetKeyDown(KeyCode.Mouse1) && PlayerInputs.instance.enableM2)
            Mouse2();
    }

    void ChangeSize(bool small)
    {
        if(small)
        {
            PlaySound(crouchSound);
            crouched = true;
            sprite.sprite = crouchSprite;
            boxCollider.size = new Vector2(boxCollider.size.x, colHeightCrouch);
            boxCollider.offset = new Vector2(boxCollider.offset.x, colOffsetCrouch);
            projectileSpawn.localPosition = new Vector3(projectileSpawn.localPosition.x, colOffsetCrouch, 0f);
        }
        else
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up, -colOffsetCrouch, LayerMask.GetMask("Default"));
            if(hit.collider == null)
            {
                PlaySound(crouchSound);
                crouched = false;
                sprite.sprite = normalSprite;
                boxCollider.size = new Vector2(boxCollider.size.x, colHeightNormal);
                boxCollider.offset = new Vector2(boxCollider.offset.x, 0f);
                projectileSpawn.localPosition = new Vector3(projectileSpawn.localPosition.x, 0f, 0f);
            }
            else
            {
                StartCoroutine(UnCrouch());
            }
        }
    }

    IEnumerator UnCrouch()
    {
        while(crouched)
        {
            if(Input.GetKeyDown(KeyCode.S) && PlayerInputs.instance.enableS)
                break;

            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up, -colOffsetCrouch, LayerMask.GetMask("Default"));
            if(hit.collider == null)
            {
                ChangeSize(false);
                break;
            }
            yield return null;
        }
    }

    void Shoot()
    {
        if(shootAble)
        {
            shootAble = false;
            PlaySound(shootingSound);
            GameObject projectileObj = Instantiate(projectilePrefab, projectileSpawn.position, Quaternion.identity);
            projectileObj.GetComponent<Rigidbody2D>().velocity = dir.direction * projectileSpeed;
            StartCoroutine(CooldownShooting());
        }
    }

    IEnumerator CooldownShooting()
    {
        yield return new WaitForSeconds(shootingCd);
        shootAble = true;
    }

    void Mouse1()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit;

        if(hit = Physics2D.Raycast(camRay.origin, camRay.direction, 100f, LayerMask.GetMask("Grabbable")))
        {
            if(hit.transform.childCount == 0)
            {
                PlaySound(grabSound);
                StartCoroutine(GrabObject(hit.transform));
            }
        }
    }

    IEnumerator GrabObject(Transform obj)
    {
        while(true)
        {
            if(obj.childCount > 0)
                break;

            if(Input.GetKeyUp(KeyCode.Mouse0) && PlayerInputs.instance.enableM1)
                break;

            Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

            Vector3 pos = camRay.origin;
            pos.z = 0f;

            obj.transform.position = pos;

            yield return null;
        }
    }

    void Mouse2()
    {
        PlaySound(createTileSound);
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        Vector3 pos = camRay.origin;
        pos.z = 0f;

        if(tileObj != null)
            Destroy(tileObj);

        tileObj = Instantiate(tilePrefab, pos, Quaternion.identity, terrain);
        Destroy(tileObj, tileLifeTime);
    }

    void PlaySound(AudioClip audioClip)
    {
        audioSource.clip = audioClip;
        audioSource.Play();
    }

    public void ResetPos()
    {
        rb.velocity = Vector2.zero;
        transform.position = startPos;
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if(col.tag != "MainCamera")
        {
            transform.SetParent(col.transform);
            onGround = true;
        }
    }
    void OnTriggerExit2D()
    {
        onGround = false;
        transform.SetParent(null);
    }
}
