﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionController : MonoBehaviour
{
    Rigidbody2D rb;
    public Vector2 direction = Vector2.right;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if(rb.velocity.x > 0f)
        {
            Quaternion newRot = transform.rotation;
            newRot.y = 0f;
            transform.rotation = newRot;
            direction = Vector2.right;
        }
        if(rb.velocity.x < 0f)
        {
            Quaternion newRot = transform.rotation;
            newRot.y = -180f;
            transform.rotation = newRot;
            direction = Vector2.left;
        }

    }
}
