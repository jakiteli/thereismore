﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneChangeManager : MonoBehaviour
{
    public float transitionTime = 0.9f;
    public Image cover;

    void Start()
    {
        StartCoroutine(UnCover());
    }

    public float NextScene()
    {
        int nextScene = SceneManager.GetActiveScene().buildIndex + 1;

        if(nextScene < SceneManager.sceneCountInBuildSettings)
            LoadLevel(nextScene);
        else
            LoadLevel(0);

        return transitionTime;
    }

    public void LoadMainMenu()
    {
        LoadLevel(0);
    }

    public void RestartLevel()
    {
        LoadLevel(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadLevel(int lvl)
    {
        StartCoroutine(CoverNLoad(lvl));
    }

    IEnumerator CoverNLoad(int lvl)
    {
        cover.gameObject.SetActive(true);
        Color targetColor = (Color.black - Color.clear) * Time.fixedDeltaTime / transitionTime;

        for(float t = transitionTime; t > 0; t -= Time.fixedDeltaTime)
        {
            cover.color += targetColor;
            yield return new WaitForFixedUpdate();
        }
        SceneManager.LoadScene(lvl);
    }

    IEnumerator UnCover()
    {
        cover.gameObject.SetActive(true);
        cover.color = Color.black;
        Color targetColor = (Color.clear - Color.black) * Time.fixedDeltaTime / transitionTime;

        for(float t = transitionTime; t > 0; t -= Time.fixedDeltaTime)
        {
            cover.color += targetColor;
            yield return new WaitForFixedUpdate();
        }
        cover.color = Color.clear;
        cover.gameObject.SetActive(false);
    }
}
