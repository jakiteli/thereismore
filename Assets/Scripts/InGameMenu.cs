﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameMenu : MonoBehaviour
{
    SceneChangeManager scenes;
    public GameObject menuObj;

    void Awake()
    {
        scenes = GetComponent<SceneChangeManager>();
    }

    void Start()
    {
        menuObj.SetActive(false);
    }

    void Update()
    {
        ListenForInput();
    }

    void ListenForInput()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            ResumeGame();
        }
    }

    public void ResumeGame()
    {
        menuObj.SetActive(!menuObj.activeSelf);
    }

    public void RestartLevel()
    {
        scenes.RestartLevel();
    }

    public void BackToMainMenu()
    {
        scenes.LoadMainMenu();
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
