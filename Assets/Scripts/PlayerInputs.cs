﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputs : MonoBehaviour
{
    public static PlayerInputs instance;
    public bool enableW, enableA, enableS, enableD, enableSpace, enableM1, enableM2;

    bool w, a, s, d, space, mouse1, mouse2;

    void Awake()
    {
        if(instance == null)
            instance = this;
        else
            Destroy(this);
    }
}
