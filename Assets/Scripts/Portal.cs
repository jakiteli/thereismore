﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public SceneChangeManager scenes;
    public AudioClip portalSound;

    AudioSource audioSource;

    bool called;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Player" && !called)
        {
            called = true;
            Destroy(GetComponent<Rigidbody2D>());
            float transitionTime = scenes.NextScene();
            StartCoroutine(PullIntoPortal(col.transform, transitionTime));
        }
    }

    IEnumerator PullIntoPortal(Transform player, float transitionTime)
    {
        PlaySound(portalSound);
        if(player.GetComponent<PlayerMovement>() != null)
            player.GetComponent<PlayerMovement>().controlled = false;
        Rigidbody2D rb = player.GetComponent<Rigidbody2D>();
        rb.isKinematic = true;
        rb.velocity = new Vector3(0f, 0f, 0f);
        Vector3 deltaPos = (transform.position - player.position) * Time.fixedDeltaTime / transitionTime;

        for(float t = transitionTime; t > 0; t -= Time.fixedDeltaTime)
        {
            player.position += deltaPos;
            yield return new WaitForFixedUpdate();
        }
    }

    void PlaySound(AudioClip audioClip)
    {
        audioSource.clip = audioClip;
        audioSource.Play();
    }
}
