﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag != "MainCamera" && col.tag != "Player")
        {
            if(col.tag == "Destroyable")
                Destroy(col.gameObject);

            Destroy(this.gameObject);
        }
    }
}
