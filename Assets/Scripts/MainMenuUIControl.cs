﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUIControl : MonoBehaviour
{
    SceneChangeManager scenes;
    public GameObject infoBox;

    void Awake()
    {
        scenes = GetComponent<SceneChangeManager>();
    }

    public void StartGame()
    {
        scenes.LoadLevel(1);
    }

    public void OpenInfo()
    {
        infoBox.SetActive(!infoBox.activeSelf);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
